import nativesockets

let
  socket = new_native_socket(sock_type = SOCK_DGRAM, protocol = IPPROTO_UDP)

proc send(filename: const cstring,
          dest: const cstring, port: cint,
          options: cint): cint =
  # Make socket
  # Bind socket
  # Send connection & initial bwp request up to CON_ATTEMPTS times
  # Receive connection response
  # Estimate BW & ERTT
  # If bad response: fail
  # While (unacked_packets && dead_periods < MAX_DEAD_PERIODS)
  #   prepare burst
  #   send burst
  #   send packet pair
  #   wait until end of burst period
  #   receive acks
  #     if no acks - increment dead_periods
  #     if ack - reset dead_periods
  #     mark each ack'd block for no-send
  #   receive bw estimation
  #   recalc ERTT & BW
  # If dead_periods == MAX_DEAD_PERIODS: die
  # Send termination ack up to TERM_ATTEMPTS times
  # Receive termination ack response
  # Close socket

proc recv(filename: const cstring,
          port: cint,
          max_size: cint,
          options: cint) =
  # Make socket
  # Bind socket
  # Listen
  # Validation connection request packet
  # If filesize > max: Send refuse packet
  # Receive BWP
  # Validate BWP
  # Send CON Accept
  # Send BWP Response
  # While (missing_blocks && dead_periods < MAX_DEAD_PERIODS)
  #   receive blocks
  #   for each block:
  #     CRC check good -> send ACK
  #   Receive BWP
  #   Validate BWP
  #   Send BWP response
  # If dead_periods == MAX_DEAD_PERIODS: die
  # While (receive block != ACK)
  # Send final ACK
  # Close socket
