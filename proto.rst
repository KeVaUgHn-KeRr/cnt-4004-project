Protocol for Reliable One-way Transfer of Objects (PROTO)
======

Design Goals
-----
* The transfer of entire files (of arbitrary size) over an unreliable network
  (UDP|IP in this case).
* Proper multiplexing of connections on the same host over the same port number.
* Convergence to optimal network utilization.
* Congestion avoidance, TCP fairness.

What PROTO is not designed for:

* It's important to understand that PROTO is not designed for communication
  over a DTN or some such extremely resource-limited network.
* It's current implementation leaves little to no room for future improvements
  (such as consuming the full 4-bit range of header types).
* Security should be handled by the application, not this protocol.

Header Forms
------
**Common**
The common header found among all others (this will also be included in the type
dependent headers shown below for posterities sake).

::
  ┌─────────┬──────────┐
  │ type: 2 │ guid: 62 │
  └─────────┴──────────┘
  ╾─────────64─────────╼

:type: The type of PROTO packet received.
:guid: A unique identifier for multiplexing.

**Connect Request**
A header for connection requests (from sender).

::
  ┌─────────┬──────────┬───────────────┬────────────────┬────────┐
  │ type: 2 │ guid: 62 │ file_size: 48 │ block_size: 15 │ dir: 1 │
  └─────────┴──────────┴───────────────┴────────────────┴────────┘
  ╾────────────────────────────128───────────────────────────────╼

:type: CON (`00`)
:guid: A unique identifier for sender-side multiplexing.
:file_size: The size, in bytes, of the file wanting to be sent.
:block_size: The size, in bytes, of each data block (does not include header).
:dir: Initial (`0`)

**Connect Response**
A header for connection responses (from receiver).

::
  ┌─────────┬──────────┬───────────┬───────────┬────────┐
  │ type: 2 │ guid: 62 │ rguid: 62 │ refuse: 1 │ dir: 1 │
  └─────────┴──────────┴───────────┴───────────┴────────┘
  ╾────────────────────────128──────────────────────────╼

:type: CON (`00`)
:guid: A unique identifier for sender-side multiplexing.
:rguid: A unique identifier for receiver-side multiplexing.
:refuse: Bit set only when the connection is refused by the receiver.
:dir: Response (`1`)

**Data**
Data packets sent by the sender.

::
  ┌─────────┬──────────┬──────────────┬──────────────────┐
  │ type: 2 │ guid: 62 │ block_id: 32 │ data: block_size │
  └─────────┴──────────┴──────────────┴──────────────────┘
  ╾────────────────96─────────────────╼╾────block_size───╼

:type: DAT (`01`)
:guid: A unique identifier for receiver-side multiplexing.
:block_id: The sequence number tracking which block is currently being sent.
:data: The data for this block, block_size in length.

**Acknowledge**
Acknowledgments of success made by the receiver.

::
  ┌─────────┬──────────┬──────────────┐
  │ type: 2 │ guid: 62 │ block_id: 32 │
  └─────────┴──────────┴──────────────┘
  ╾───────────────96──────────────────╼

:type: ACK (`10`)
:guid: A unique identifier for sender-side multiplexing.
:block_id: The sequence number for which block was received.

**Bandwidth Estimator (Request)**
Form for each of the packet pairs.

::
  ┌─────────┬──────────┬───────────────┬───────────┬──────────┐
  │ type: 2 │ guid: 62 │ packet_id: 32 │ eldest: 1 │ padd: 31 │
  └─────────┴──────────┴───────────────┴───────────┴──────────┘
  ╾──────────────────────────128──────────────────────────────╼

:type: BWP (`11`)
:guid: A unique identifier for receiver-side multiplexing.
:packet_id: The identifier for the packet pair this BWP packet belongs to.
:eldest: This bit is set only for the second BWP packet sent.
:padding: Currently not in use!

**Bandwidth Estimator (Response)**
Form for the packet pair response.

::
  ┌─────────┬──────────┬───────────────┬──────────┐
  │ type: 2 │ guid: 62 │ packet_id: 32 │ nsec: 32 │
  └─────────┴──────────┴───────────────┴──────────┘
  ╾────────────────────128────────────────────────╼

:type: BWP (`11`)
:guid: A unique identifier for sender-side multiplexing.
:packet_id: An identifier for the packet pair this BWP response is to.
:nsec: The time spacing between the packet pair accepted by the receiver.

Interface
-----
`int sendFile(char *fileName, char *destIpAddr, int destPortNum, int options);`
~~~~~
Transmit a file to another host on the network.

:Contracts:
  * fileName is not null.
  * fileName is an existing file of non-zero size.
  * destIpAddr is not null.
:Inputs:
  :filename: The relative filename to be sent.
  :destIPAddr:
  :destPorNum: The port to establish a connection on.
  :options: To be defined.
:Return: -1 on failure, 0 on success.

`int recvFile(char *fileName, int portNum, int maxSize, int options);`
~~~~~
Listen for and receive a file from another host on the network.

:Contracts:
  * fileName is not null.
  * fileName is an accessible filepath.
  * maxSize is not 0.
:Inputs:
  :filename: The relative filename to save to.
  :portNum: The port to establish a connection on.
  :maxSize: The max size file that can be accepted.
  :options: To be defined.
:Return: -1 on failure, 0 on success.

Environment Settings
~~~~~~
:connect_attmpts:
:retry_attempts:
:decrease_factor: ADIMD decrease factor.
:sample_smoothing: Kernel Density Height Modifier

Establishing a Connection (2-way)
------
The sender sends a connect message to the receiver via UDP. The header of this
packet will contain a unique GUID for the sender host's multiplexing. Also
included is the 48-bit total file size and the 15-bit block size (the size of
each data packet). The last bit of the packet header is set to 0, as to indicate
a connect request rather than response.

Directly after sending this connect message, a bandwidth packet pair will be
broadcast with GUID = `0` and a random packet_id (which will be used for the
starting block_id). Though the receiver may not know to which connection request
this packet pair belongs to *on a single IP*, it's harmless to confuse as long as
it only uses it for connections *on that IP*. Even on a mobile network, asking
that 3 packets receive on the same IP is acceptable. With this, a first guess is
made as to the bandwidth of our network.

If, in any of the above steps a packet is dropped or received in the wrong order,
the receiver will simply ignore the request and stay silent. This will prompt a
timeout at the sender host, which will reattempt up to it's user-set retry limit.
If all goes well but the filesize is too large for the receiver to accept, the
receiver sends back a connection response with the refuse bit set to 1.

On acceping the connection, the receiver generates it's own multiplexing GUID.
A connection packet is sent indicating success with the sender's guid first and
the receiver's second. The last bit in the packet header is set to 1 to indicate
a connect response rather than request.

Directly after that, the receiver sends the first BWP response packet. Again,
the guid and packet_id are those received during connection. The nsecs field is
filled with the number of nanoseconds measured between the received packet pair.

Just like before, if one or both of these packets are lost the sender restarts
the process. The receiver will recognize this case when, after sending the
ACK and BWP packets it receives a connection request on the same GUID. It can
simply retransmit these two packets *even if the duplicate connection request
has bad BWP pairs*.

Transferring Data
------
Once the connection is fully established, the sender begins to transmit blocks
of data during the broadcast period (as outline in `Congestion Control and TCP
Fairness`_).

Each implementation is free to choose how the sender manages what blocks are
still required. In this reference implementation, we make use of a linked list
of (first_block, gap_length) nodes to show where gaps are in acknowledged data.
This allows us to severely reduce memory requirements and take advantage of the
"bursty" nature of networking noise / loss.

Upon arrival of non-corrupted data, the receiver checks if this is a duplicate
or invalid block. If so, the block is ignored. Otherwise the block is accepted
and can be copied into the file (at the discretion of the implementation). To
inform the sender of success, an acknowledge packet is produces with the sender's
GUID and the block_id that was received.

When the sender receives the ACK packet, it removes the block from the "still
required" block queue so it is not retransmitted.

Data Narrowing
~~~~~~
As the file is sent, the amount of unique data available to be sent falls. At
some point in transfer, we will no longer be able to transmit completely unique
data during the whole transfer window. For this reason, the PROTO specification
requires implementations to, in some way, prevent broadcasting duplicate blocks
in the same broadcasting period. This reference implementation accomplishes this
goal by marking the first packet sent in the broadcast and prematurely quitting
if that block is reached again.

Congestion Control and TCP Fairness
------

RTT Determination
~~~~~~

Bandwidth Estimation by Packet Pairs
~~~~~~
[https://goo.gl/MK7Aom]

Once a bandwidth estimation is made, we're able to determine packet pacing.
The exact method (send intervals or burst size) is up to the implementation, but
the reference implementation makes the assumption of a ticking kernel that does
not have a nanosecond resolution timer and utilizes burst segments.

Bursts are sent in some user-defined interval. By taking the number of bursts
per second and multiplying by the bandwidth over blcok_size, we get the number
of packets to send per burst. Each burst interval, we begin by transmitting this
number of blocks or until the next interval starts (whichever comes first).
Immediately after a BWP pair is sent to request a bandwidth update.

Receiver Bottlenecking
~~~~~~
Though packet pairs work to estimate the bottleneck bandwidth across the network,
it fails to identify bottlenecks at the receiver itself. 

Accounting for Lossyness
~~~~~~

Terminating a Connection (2-way)
------
When all blocks have been received, it's time for the connection to be
terminated. Since this data is not being streamed, we have the privilege of
having both sides aware that all blocks have been sent/received after the
last acknowledge has made it's way to the sender. On arrival of that packet
the sender publishes it's own acknowledge packet with the max block_id + 1,
a signal that it knows the file has been transmitted. The receiver echos this
acknowledge packet back to the sender and immediately closes it's connection.

If either the sender or receiver acknowledge packets are lost, the sender will
attempt to retry after a short timeout period. If the maximum number of retries
fail, it timeouts and implicitly assumes that the receiver has closed and the
final ACK packet was simply lost.